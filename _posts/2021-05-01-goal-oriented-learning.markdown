---
layout: post
title:  "Goal-oriented learning - a way to represent a task"
date:   2021-05-01 14:04:42 +0100
categories: ai
comments: true
excerpt: Goal-oriented learning allows the agent to learn tasks faster.
---

Goal-oriented learning (GOL) is about learning by aiming to achieve a goal. The goal is given as part of the input (alternatively, it's a question to answer instead of a goal). The agent can be also given statements describing the environment and how the actions impact the environment.

Human learns a big percentage of what they learn by aiming to achieve a goal.

GOL is a good fit for tasks like a logical riddle or a mathematical problem.

# Why goal-oriented learning

Because it lowers the amount of time the agent needs to learn a task.

<details>
  <summary>Example</summary>

  Imagine that you are AI. The input is "3 2 4 5". You must output a number. What is your answer?

  If you're not given a goal, then you must take a guess. You must take many of such guesses until you can give correct answers regularly.

  Now you are given the same output, but you are also told that the goal is to output the second biggest number. Now you immediately know that the answer is "4" which saves a lot of time (presuming that you have some prior knowledge).
</details>

# Description

An abstract class representing a GOL agent can look like this:

```python
from abc import ABC
from dataclasses import dataclass
from typing import Any, Optional, Union


@dataclass
class Literal:
    value: Any


@dataclass
class Expression:
    predicate: str,
    arguments: list[Union[Expression|Literal]]


@dataclass
class Task:
    """Task to solve.

    Args:
        observation: Observation of the agent (input of the task).
        actions: Actions the agent can execute. They can have any
            number of parameters of any type. The actions can have
            impact on observation (their execution might change the
            value of `observation` property).
        statements: Description of the task in a form of
            declarative statements represented as expressions. Each
            declarative statement means that the expression
            evaluates to True. One of the statements must specify
            the goal to achieve or the question to answer.
    """
    observation: Any,
    actions: list[callable],
    statements: list[Expression]


class GolAgent(ABC):
    @abstractmethod
    def solve(self, task: Task, time: int) -> Any:
        """Accomplish the goal / answer the question.

        Args:
            task: Task to solve.
            time: How much time the agent can spend on the task
                maximally. Expressed in the number of operations the
                agent can make.

        Returns:
            If the task is to answer a question, then it returns the
            answer (can be of any type). Otherwise - None.
        """
        pass

    @abstractmethod
    def evaluate(self, task: Task, score: float):
        """Inform the agent how well it did on the task.

        Args:
            task: Evaluated task.
            score: Number representing how well it did.

        Raises:
            ValueError: If the task hasn't been passed to the solve()
            method before.
        """
        pass
```

# The end

I'm happy to answer additional questions, for example:
1. Please give an example of how the above GolAgent class could be used to accomplish a specific task.
2. How to get any reinforcement learning algorithm to be able to do GOL tasks?
3. How to represent a goal or a question with an expression?

# Citation

```
@article{czapiewski2021GOL,
  title   = "Goal-oriented learning - a way to represent a task",
  author  = "Damian Czapiewski",
  journal = "damc4.gitlab.io",
  year    = "2021",
  url     = "https://damc4.gitlab.io/ai/goal-oriented-learning/"
}
```
